const jwt = require('jsonwebtoken')
const fs = require('fs')
var connection = require('../config/database');

const authorization = ((req, res, next) => {
    const tokenheader = req.headers['authorization']//.split(' ')[1]
    connection.query('SELECT token FROM tbl_token WHERE delete_at = "NULL"', async function (error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results.length > 0) {
                var count = [results.length];
                for (var i = 0; i < count; i++) {
                    const comparision = (tokenheader == results[i].token);
                    if (comparision) {
                        const privateKey = fs.readFileSync('config/private.key')
                        jwt.verify(tokenheader, privateKey, function (error, decoded) {
                            if (error) return res.status(401).send({"message": "Unauthorized"})
                            console.log(error)
                            console.log(decoded)
                            if (decoded.role === undefined || decoded.role !== 'Admin') 
                            return res.status(403).send({"message": "Forbidden"})
                            next()
                        })
                    }
                }
            }
        }
    });
});


module.exports = authorization   