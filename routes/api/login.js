const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const fs = require('fs')
const bcrypt = require('bcrypt');
var connection = require('../../config/database');



router.post('/', function (req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
    connection.query('SELECT password,role,member_id FROM tbl_member WHERE username = ? AND delete_at = "NULL"', [username], async function (error, results, fields) {
        if (error) {
               res.status(400).send ({"message":"error ocurred"})    
        } else {
            if (results.length > 0) {
                const comparision = await bcrypt.compareSync(password,results[0].password) 
                //const comparision = (password == results[0].password)
                if (comparision) {
                    const privateKey = fs.readFileSync('config/private.key');
                    const payload = {
                        id: results[0].member_id,
                        name: {username} ,
                        role: results[0].role
                    }
                    const token = jwt.sign(payload, privateKey); 
                    res.send({
                        "message": "login sucessfull",
                        "Token": token
                    }) 
                    var savetoken = {
                        "token" : token,
                        "member_id" : results[0].member_id,
                        "created_by" : results[0].role
                    }
                    connection.query('INSERT INTO tbl_token SET ?', savetoken, function (error, result, fields) {
                        if (error) throw error;
                    });
                }
                else {
                    res.send({"message":"Incorrect username or password"});
                }
            }
            else {
                res.send({"message":"Incorrect username or password"});
            }
        }
    });

})

module.exports = router

