var router = require('express').Router();
var connection = require('../../config/database');
const jwt = require('jsonwebtoken')
const fs = require('fs')
var moment = require('moment'); // require


router.get('/checkin', (req, res) => {
    const tokenheader = req.headers['authorization']//.split(' ')[1]
    connection.query('SELECT token FROM tbl_token WHERE delete_at = "NULL"', async function (error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results.length > 0) {
                var count = [results.length];
                for (var i = 0; i < count; i++) {
                    const comparision = (tokenheader == results[i].token);
                    if (comparision) {
                        const privateKey = fs.readFileSync('config/private.key')
                        jwt.verify(tokenheader, privateKey, function (error, decode) {
                            if (decode.id === undefined) {
                                return res.status(403)
                            } else {
                                function status() { 
                                    let result;
                                    if (new Date().getHours().toString() < 10) {
                                        result = 'ATTEND';
                                    }else if (new Date().getHours().toString() == 10 & new Date().getMinutes().toString() <= 15){
                                        result = 'ATTEND';
                                    } else if (new Date().getHours().toString() < 12) {
                                        result = 'LATE';
                                    } else if (new Date().getHours().toString() > 12){
                                        result = 'ABSENCE';
                                    }
                                    return result;
                                }

                                var detail = {
                                    "checkin_at": moment().utcOffset('+0700').format('YYYY-MM-DD HH:mm:ss'),
                                    "created_at": new Date().toString().split(' ')[0] + new Date().toISOString().slice(0, 10),
                                    "member_id": decode.id,
                                    "status": status()
                                }
                                connection.query('INSERT INTO tbl_checkstatus SET ? ', [detail], async function (error, results, fields) {
                                    if (error) throw error;
                                    //console.log(moment().utcOffset('+0700').format('YYYY-MM-DD HH:MM:SS'))
                                    res.send({ "message": "Check-in sucessfull", 
                                    "status":status()
                                });

                                });
                            }
                        });
                    }
                }
            }
        }
    });
});

router.get('/checkout', (req, res) => {
    var checkout_at = moment().utcOffset('+0700').format('YYYY-MM-DD HH:mm:ss');
    var created_at = new Date().toString().split(' ')[0] + new Date().toISOString().slice(0, 10);
    const tokenheader = req.headers['authorization']//.split(' ')[1]
    connection.query('SELECT token FROM tbl_token WHERE delete_at = "NULL"', async function (error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results.length > 0) {
                var count = [results.length];
                for (var i = 0; i < count; i++) {
                    const comparision = (tokenheader == results[i].token);
                    if (comparision) {
                        const privateKey = fs.readFileSync('config/private.key')
                        jwt.verify(tokenheader, privateKey, function (error, decode) {
                            if (decode.id === undefined) {
                                return res.status(403)
                            } else {
                                connection.query('UPDATE tbl_checkstatus SET checkout_at = ? WHERE member_id = ? AND created_at = ? ', [checkout_at, decode.id, created_at], async function (error, results, fields) {
                                    if (error) throw error;
                                    res.send({ "message": "Check-out sucessfull" });
                                });
                            }
                        });
                    }
                }
            }
        }
    });
});


router.get('/profile', (req, res) => {
    const tokenheader = req.headers['authorization']//.split(' ')[1]
    connection.query('SELECT token FROM tbl_token WHERE delete_at = "NULL"', async function (error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results.length > 0) {
                var count = [results.length];
                for (var i = 0; i < count; i++) {
                    const comparision = (tokenheader == results[i].token);
                    if (comparision) {
                        const privateKey = fs.readFileSync('config/private.key')
                        jwt.verify(tokenheader, privateKey, function (error, decode) {
                            if (decode.id === undefined) {
                                return res.status(403)
                            } else {
                                connection.query('SELECT username,firstname,lastname,position,nickname,image FROM tbl_member WHERE member_id = ?', [decode.id], async function (error, results, fields) {
                                    if (error) throw error;
                                    res.json(results)
                                });
                            }
                        });
                    }
                }
            }
        }
    });
});


router.get('/history/:page', (req, res) => {
    const tokenheader = req.headers['authorization'];//.split(' ')[1]
    connection.query('SELECT token FROM tbl_token WHERE delete_at = "NULL"', async function (error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results.length > 0) {
                var count = [results.length];
                for (var i = 0; i < count; i++) {
                    const comparision = (tokenheader == results[i].token);
                    if (comparision) {
                        const privateKey = fs.readFileSync('config/private.key')
                        jwt.verify(tokenheader, privateKey, function (error, decode) {
                            if (decode.id === undefined) {
                                return res.status(403)
                            } else {
                                var limit = 7;
                                const page =  req.params.page;
                                var offset = (page - 1) * limit;
                                connection.query('SELECT checkin_at,checkout_at,note,status,created_at FROM tbl_checkstatus WHERE member_id = ?  ORDER BY check_id DESC LIMIT ? OFFSET ?  ', [decode.id,limit,offset], async function (error, results, fields) {
                                    if (error) throw error;
                                    res.json(results)
                                });
                            }
                        });
                    }
                }
            }
        }
    });
});


router.get('/logout', (req, res) => {
    const tokenheader = req.headers['authorization']
    connection.query('SELECT token FROM tbl_token WHERE delete_at = "NULL"', async function (error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results.length > 0) {
                var count = [results.length];
                for (var i = 0; i < count; i++) {
                    const comparision = (tokenheader == results[i].token);
                    if (comparision) {
                        const privateKey = fs.readFileSync('config/private.key')
                        jwt.verify(tokenheader, privateKey, function (error, decode) {
                            if (decode.id === undefined) {
                                return res.status(403)
                            } else {
                                var detaildelete = {
                                    "delete_at": new Date().toISOString().slice(0, 10),
                                    "delete_by": decode.role
                                }
                                connection.query('UPDATE tbl_token SET ? WHERE member_id = ?', [detaildelete, decode.id], async function (error, results, fields) {
                                    if (error) throw error;
                                    res.send({ "message": "logout Successfull" })
                                });
                            }
                        });
                    }
                }
            }
        }
    });
});


module.exports = router