var connection = require('../../config/database');
var router = require('express').Router();
var generator = require('generate-password');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const authorization = require('../auth');
const jwt = require('jsonwebtoken');
var moment = require('moment'); // require
const fs = require('fs');
var multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function (req, file, cb) {
        cb(null, new Date().getSeconds() + '-' + file.originalname);
    }
});
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};
const upload = multer({ storage: storage, limits: { fileSize: 1024 * 1024 * 5 }, fileFilter: fileFilter });

//router.post('/upload', upload.single('photo'), (req, res) => {
//var image = req.file.path
//res.json({"path":image})
//});

//dashbord
router.get('/showallhistory', authorization, (req, res) => {
    var created_at = new Date().toString().split(' ')[0] + new Date().toISOString().slice(0, 10);
    connection.query('SELECT tbl_member.member_id AS id,tbl_member.firstname AS name,tbl_checkstatus.status AS status,tbl_checkstatus.created_at AS Date,tbl_checkstatus.checkin_at AS checkin,tbl_checkstatus.note AS note FROM tbl_member LEFT JOIN tbl_checkstatus ON tbl_member.member_id=tbl_checkstatus.member_id AND tbl_checkstatus.created_at= ? WHERE tbl_member.role="User" ORDER BY tbl_member.member_id ASC ', [created_at], function (error, results, fields) {
        if (error) throw error;

        res.json(results);
    });
});




router.get('/showmember', authorization, (req, res) => {
    connection.query('SELECT member_id,username,firstname,lastname,nickname,position,image,role,email FROM tbl_member WHERE delete_at = "NULL" AND role = "User"', function (error, results, fields) {
        if (error) throw error;
        res.json(results)
    });
});


router.get('/showmemberhistory/:id', authorization, (req, res) => {
    var member_id = req.params.id;
    connection.query('SELECT * FROM tbl_checkstatus WHERE member_id = ? ORDER BY check_id DESC', [member_id], function (error, results, fields) {
        if (error) throw error;

        res.json(results)
    });
});


router.post('/edit', authorization, upload.single('photo'), (req, res) => {
    var name = {
        "firstname": req.body.firstname,
        "lastname": req.body.lastname,
        "position": req.body.position,
        "nickname": req.body.nickname,
        "email": req.body.email,
        "image": req.file.path,
        "update_at": new Date().toISOString().slice(0, 10),
        "update_by": "Admin"
    }
    var member_id = req.body.member_id;
    connection.query('UPDATE tbl_member SET ? WHERE member_id = ?', [name, member_id], async function (error, results, fields) {
        if (error) throw error;
        res.send({ "message": "edit sucessfull" });
    });
});


router.get('/delete/:id', authorization, (req, res) => {
    var detaildelete = {
        "delete_at": new Date().toISOString().slice(0, 10),
        "delete_by": "Admin"
    }
    var member_id = req.params.id;
    connection.query('UPDATE tbl_member SET ? WHERE member_id = ?', [detaildelete, member_id], async function (error, result, fields) {
        if (error) throw error;
        res.send({ "message": "delete Successfull" })
        console.log("deleted: " + result.affectedRows + " records");
    });
});


router.post('/addleave/:id', authorization, (req, res) => {
    var details = {
        "note": req.body.note,
        "member_id": req.params.id,
        "created_at": new Date().toString().split(' ')[0] + new Date().toISOString().slice(0, 10),
        "created_by": "Admin",
        "status": req.body.status
    }
    connection.query('INSERT INTO tbl_checkstatus SET ?', details, function (error, result, fields) {
        if (error) throw error;
        res.status(201).send({ "message": "add leave sucessfull" });
    });
});


router.post('/addmember', upload.single('photo'), async function (req, res) {
    var passwordgen = generator.generate({
        length: 4,
        numbers: false
    });
    const password = passwordgen;
    const encryptedPassword = await bcrypt.hashSync(password, saltRounds)
    var users = {
        //"member_id": req.body.member_id,
        "username": req.body.username,
        "password": encryptedPassword,
        "firstname": req.body.firstname,
        "lastname": req.body.lastname,
        "position": req.body.position,
        "nickname": req.body.nickname,
        "email": req.body.email,
        "image": req.file.path,
        "created_at":moment().utcOffset('+0700').format('YYYY-MM-DD HH:mm:ss'),
        "role": "User",
        "created_by": "Admin"
    }
    connection.query('INSERT INTO tbl_member SET ?', users, function (error, results, fields) {
        if (error) {
            res.status(400).send({ "message": "error ocurred" })
        } else {
            res.status(201).send({ "message": "add member sucessfull", password });
        }
    });
});


router.get('/logout', (req, res) => {
    const tokenheader = req.headers['authorization']
    connection.query('SELECT token FROM tbl_token WHERE delete_at = "NULL"', async function (error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results.length > 0) {
                var count = [results.length];
                for (var i = 0; i < count; i++) {
                    const comparision = (tokenheader == results[i].token);
                    if (comparision) {
                        const privateKey = fs.readFileSync('config/private.key')
                        jwt.verify(tokenheader, privateKey, function (error, decode) {
                            if (decode.id === undefined) {
                                return res.status(403)
                            } else {
                                var detaildelete = {
                                    "delete_at": new Date().toISOString().slice(0, 10),
                                    "delete_by": decode.role
                                }
                                connection.query('UPDATE tbl_token SET ? WHERE member_id = ?', [detaildelete, decode.id], async function (error, results, fields) {
                                    if (error) throw error;
                                    res.send({ "message": "logout Successfull" })
                                });
                            }
                        });
                    }
                }
            }
        }
    });
});
module.exports = router